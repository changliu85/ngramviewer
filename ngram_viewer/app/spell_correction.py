"""
SpellCorrector

Author:
    Chang Liu, changliu.cee@gmail.com

Description:
    SpellCorrector class that wraps up code in https://github.com/mattalcock/
blog/blob/master/2012/12/5/python-spell-checker.rst, courtesy of mattalcock.

Usage:
    # init
    sc = SpellCorrector(model)
    # correct
sc.correct(phrase)
"""


class SpellCorrector():
    def __init__(self, model):
        self.model = model
        self.alphabet = 'abcdefghijklmnopqrstuvwxyz'

    def _edits1(self, word):
        """Return candidates that are one edit away from word."""
        s = [(word[:i], word[i:]) for i in range(len(word) + 1)]
        deletes    = [a + b[1:] for a, b in s if b]
        transposes = [a + b[1] + b[0] + b[2:] for a, b in s if len(b) > 1]
        replaces   = [a + c + b[1:] for a, b in s for c in self.alphabet if b]
        inserts    = [a + c + b     for a, b in s for c in self.alphabet]
        return set(deletes + transposes + replaces + inserts)

    def _known_edits2(self, word):
        """Return candidates that are one or two edits away from word and is in model."""
        return set(e2 for e1 in self._edits1(word) for e2 in self._edits1(e1) if e2 in self.model)

    def _known(self, words):
        """Return candidates that is in model."""
        return set(w for w in words if w in self.model)

    def correct(self, word):
        """Return corrected word."""
        candidates = (self._known([word]) or
                      self._known(self._edits1(word)) or
                      self._known_edits2(word) or
                      [word])
        correction = max(candidates, key=self.model.get)
        return correction, correction != word
