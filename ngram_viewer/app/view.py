# from flask import make_response
# from flask import session
from flask import jsonify
from flask import render_template
from flask import request
# from flask import redirect

from . import app


@app.route('/', methods=['GET'])
@app.route('/<text>', methods=['GET'])
def index(text=""):
    return render_template('index.html', text=text)


@app.route('/complete', methods=['GET'])
def complete():
    pre = request.args.get('pre')
    res = app.ni.get_autocomplete(pre)
    return jsonify(res)


@app.route('/fetch', methods=['GET'])
def fetch():
    query = request.args.get('query')
    data = {}
    phrases = [phrase.strip().lower() for phrase in query.split(',')]
    corrections = []
    for i, phrase in enumerate(phrases):
        phrase, is_changed = app.sc.correct(phrase)
        if is_changed:
            corrections.append((phrases[i], phrase))
            phrases[i] = phrase
        trend = app.ni.get_phrase_trend(phrase)
        for year in trend:
            if year not in data:
                data[year] = {'year': str(year), phrase: trend[year]}
            else:
                data[year].update({phrase: trend[year]})

    res = {
        'data': sorted(data.values(), key=lambda d: d['year']),
        'xkey': 'year',
        'phrases': phrases,
        'corrections': corrections
    }
    print res
    return jsonify(res)


@app.route('/corret', methods=['GET'])
def correct():
    word = request.args.get('word')
    return app.sc.correct(word)
