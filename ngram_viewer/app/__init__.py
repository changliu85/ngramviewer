import os
import logging
import sys

from flask import Flask
from flask_session import Session
import redis

from .ngram_indexer import NgramIndexer
from .spell_correction import SpellCorrector

app = Flask(__name__)

# Load config.
try:
    if os.environ['NGRAM_ENVIRONMENT'] == 'prod':
        app.config.from_object('app.config.ProductionConfig')
    elif os.environ['NGRAM_ENVIRONMENT'] == 'dev':
        app.config.from_object('app.config.DevelopmentConfig')
    else:
        raise ValueError(
            'Please set environment variable(INSIGHT_ENVIRONMENT) to Production, Development.')
except KeyError:
    app.config.from_object('app.config.DevelopmentConfig')

# Redis connections.
app.rconn = redis.StrictRedis(
    host=app.config['REDIS_DB_ADDR'],
    port=app.config.get('REDIS_PORT'),
    db=app.config.get('REDIS_DB_NUM'),
    )

rconn_session = redis.StrictRedis(
    host=app.config['REDIS_DB_ADDR'],
    port=app.config.get('REDIS_PORT'),
    db=app.config.get('REDIS_SESSION_NUM'),
    )

# Init logger.
app.mylogger = logging.getLogger('amazonInsight')
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s')
handler.setFormatter(formatter)
app.mylogger.addHandler(handler)
app.mylogger.setLevel(logging.DEBUG)

# Init Session.
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]xWX/,?RT'
app.config['SESSION_TYPE'] = 'redis'
app.config['SESSION_REDIS'] = rconn_session
Session(app)

# NgramIndexer
app.ni = NgramIndexer(
    prefix=app.config.get('PREFIX'),
    rconn=app.rconn,
    accepted_extensions=app.config.get('ACCEPTED_EXT'),
    )

# SpellCorrector
model = dict(app.rconn.zrange(
    name=app.config['PREFIX']+'freq',
    start=0,
    end=-1,
    withscores=True))
app.sc = SpellCorrector(model)

# Load views.
from . import view
