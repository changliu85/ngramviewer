document.getElementById('searchBox').onkeydown = function(e){
  if (!e) e = window.event;
  var keyCode = e.keyCode || e.which;
  console.log(keyCode);
  if (keyCode == '13'){
    console.log('enter pressed', $('#searchBox').val());
    $('.acContainer').hide();

    $.ajax({
      url : "/fetch",
      type : "GET",
      data: {'query': $('#searchBox').val()},
      success : function(res) {
        console.log(res);
        $('#chart').empty();
        Morris.Line({
          element: 'chart',
          data: res.data,
          xkey: res.xkey,
          ykeys: res.phrases,
          labels: res.phrases,
        });

        res.corrections.forEach(function(pair) {
          $('.alerts').empty();
          $('.alerts').append('' +
            '<div class="alert alert-warning alert-dismissible in" role="alert">' +
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
              '</button>' +
              '<i class="fa fa-exclamation-triangle" aria-hidden="true"> </i>' +
              'Showing results with <span class="corrected">' + pair[1] + '</span> instead of <span class="be-corrected">' + pair[0] +
            '</span></div>');
          $('.alerts').show();
        });
      },
      error : function(error) {
        console.log(error);
      }
    });

  } else {
    // Auto complete
    var pre = $('#searchBox').val();
    if (keyCode == '8' || keyCode == '46') {
      pre = pre.slice(0,pre.length-1);
    } else {
      pre = pre + String.fromCharCode(keyCode).toLowerCase();
    }
    console.log(pre);
    $.ajax({
      url : "/complete",
      type : "GET",
      data: {'pre': pre},
      success : function(response) {
        // console.log(response);
        $('.acContainer').empty();
        response.forEach(function(phrase, i) {
          $('.acContainer').append('' +
            '<div class="btn btn-default acOption" onClick="">' +
              phrase +
            '</div>');
        });
      },
      error : function(error) {
        console.log(error);
      }
    });
  }
};

$(window).click(function() {
  $('#searchBox').parent().removeClass('selected');
  $('#searchBox').attr("placeholder", "Input n-grams separated by comma (1 <= n <= 5)");
  $('.acContainer').hide();
});

$('#searchBox').click(function(event){
    event.stopPropagation();
});

function inputing(){
  $('.alerts').hide();
  $('.acContainer').show();
  // console.log($('#searchBox').parent());
  $('#searchBox').parent().addClass('selected');
  $('#searchBox').attr("placeholder", "");
}
