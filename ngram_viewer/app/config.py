import os


class Config(object):
    VERSION = 'v1'
    REDIS_PORT = 6379
    REDIS_DB_NUM = 1
    REDIS_SESSION_NUM = 2
    DEBUG = True
    PREFIX = 'codetest:'
    ACCEPTED_EXT = ['.txt', '.txt.utf-8']


class ProductionConfig(Config):
    REDIS_DB_ADDR = os.environ.get('REDIS_DB_ADDR')
    DEBUG = False


class DevelopmentConfig(Config):
    REDIS_DB_ADDR = os.environ.get('REDIS_DB_ADDR', 'localhost')
