"""
NgramIndexer

Author:
    Chang Liu, changliu.cee@gmail.com

Description:
    Implemented NgramIndexer class to index ngrams extracted from txt files.
Indices are stored in a redis db using sorted sets (z-sets), brokedown by year
of book. Another sorted set is used for auto-complete and auto-correction.

Usage:
    # init
    ni = NgramIndexer(prefix, rconn, accepted_extensions)
    # read
    ni.read_from_path(path)
    # get auto complete
    ni.get_autocomplete(word)
    # get ngram appearance by year
    ni.get_phrase_trend(phrase)
"""
import codecs
from collections import Counter
from itertools import chain
import os
import string

from nltk import ngrams
from nltk import word_tokenize
from nltk.tokenize.regexp import RegexpTokenizer
import redis


puncts = set(string.punctuation)


class NgramIndexer():
    def __init__(self, prefix, rconn, accepted_extensions):
        self.prefix = prefix
        self.rconn = rconn
        self.accepted_extensions = accepted_extensions

    @property
    def years(self):
        """Return all years indexed."""
        return self.rconn.zrange(self.prefix + 'years', 0, -1)

    @property
    def files(self):
        """Return all filenames indexed."""
        return self.rconn.zrange(self.prefix + 'files', 0, -1)

    def read_from_path(self, path):
        """Read and index all files with accepted extensions from folder path."""
        for (dirpath, dirnames, filenames) in os.walk(path):
            if dirnames:
                # skip directories.
                continue

            year = int(dirpath.split('/')[-1])
            self.rconn.zadd(self.prefix + 'years', 0, year)

            print '\n\nReading and indexing book of %s' % year

            for filename in filenames:
                # Check extensions.
                if any(filename.endswith(ext)
                       for ext in self.accepted_extensions):

                    fpath = os.sep.join([dirpath, filename])
                    self.read_and_index(fpath, year)

    def read_and_index(self, fpath, year):
        """Read and index one file given path of file."""
        content = self._read_content(fpath, year)
        if content:
            self._index_content(content, year)
        return None

    def _read_content(self, fpath, year):
        """Read content of a file given path of file."""
        filename = fpath.split('/')[-1]
        if filename in self.rconn.zrangebyscore(self.prefix + 'files', year, year):
            print '\t file already loaded'
            return -1

        print '\treading %s' % fpath
        with codecs.open(fpath, encoding='utf-8') as f:
            BOM = codecs.BOM_UTF8.decode('utf8')
            content = f.read().lstrip(BOM).lower()

        self.rconn.zadd(self.prefix + 'files', year, filename)

        return content

    def _index_content(self, content, year):
        """Extract 2grams to 5grams from content and index them by calling _index_gram."""
        word_list = self._word_punct_tokenize(content)
        self._index_gram(list(chain.from_iterable(word_list)), year)

        for n in xrange(2, 6):
            grams = [' '.join(gram)
                     for words in word_list
                     for gram in ngrams(words, n)]
            print '\t\tIndexing %s %s-grams' % (len(grams), n)
            self._index_gram(grams, year)

    def _index_gram(self, grams, year):
        """Index grams by year and by frequency."""
        index_freq = self.prefix + 'freq'
        index_gram = self.prefix + 'ngrams:%s' % year
        for phrase, count in Counter(grams).iteritems():
            pipe = self.rconn.pipeline()
            pipe.zincrby(index_freq, phrase, count)
            old_count = self._get_phrase_count(phrase, year)
            if old_count:
                pipe.zrem(index_gram, 0, '%s:%s' % (phrase, old_count))
                count += old_count

            pipe.zadd(index_gram, 0, '%s:%s' % (phrase, count))
            pipe.execute()

    def get_phrase_trend(self, phrase):
        """Get trend of ngrams over years."""
        res = {}
        for year in xrange(2012, 2017):
            count = self._get_phrase_count(phrase, year)
            res[year] = count
        return res

    def _get_phrase_count(self, phrase, year):
        """Get count of ngrams in year."""
        phrase_count = self.rconn.zrangebylex(
            name=self.prefix + 'ngrams:%s' % year,
            min=u'[%s:' % phrase,
            max=u'[%s:\xff' % phrase,
            )
        if phrase_count:
            count = int(phrase_count[0].split(':')[-1])
        else:
            count = 0

        return count

    def get_autocomplete(self, user_input):
        """Auto complete user input."""
        pre = user_input.split(',')[-1].strip()
        if not pre:
            return []
        res = self.rconn.zrangebylex(
            name=self.prefix+'freq',
            min=u'[%s' % pre,
            max=u'[%s\xff' % pre,
            start=0,
            num=10,
            )
        return [r.split(':')[0] for r in res]

    def _word_punct_tokenize(self, content):
        """Tokenize content first by punctuations and then by word,
        so that words across punctuations won't appear in one ngram.
        """
        pattern = '''[!"#$%&'()*+,./:;<=>?@\^_`{|}~-]|[\r\n]'''
        tokenizer = RegexpTokenizer(pattern, gaps=True)
        toks = [[w for w in word_tokenize(sent)]
                for sent in tokenizer.tokenize(content)]
        return toks

    def phrase_2_idx(self, phrase):
        return ' '.join(self._word_2_idx(w) for w in phrase)

    def idx_2_phrase(self, idx):
        return ' '.join(self._idx_2_word(w) for w in idx)

    def _word_2_idx(self, word):
        idx = self.rconn.zscore(self.prefix + 'word_idx', word)
        if idx:
            return idx

        max_idx = self.rconn.zrevrange(
            name=self.prefix + 'word_idx',
            start=0,
            end=0,
            withscores=True,
            )
        if max_idx:
            max_idx = max_idx[0][1] + 1
        else:
            max_idx = 0

        self.rconn.zadd(self.prefix + 'word_idx', max_idx, word)

        return max_idx

    def _idx_2_word(self, idx):
        word = self.rconn.zrangebyscore(self.prefix + 'word_idx', idx, idx)
        if word:
            return word[0]
        else:
            return None

    def clean_low_freqs_entry(self):
        """Clear low freqs entries so that they won't show up in autocomplete."""
        lowfreqs = self.rconn.zrangebyscore(name=ni.prefix+'freq', min=0, max=2)
        start, chunk = 0, 10000
        while start < len(lowfreqs):
            self.rconn.zrem(ni.prefix+'freq', *lowfreqs[start:(start+chunk)])
            start += chunk


def ispunct(w):
    return all(c in set(string.punctuation) for c in w)


if __name__ == '__main__':
    prefix = 'codetest:'
    rconn = redis.StrictRedis(db=1)
    accepted_extensions = ['.txt', '.txt.utf-8']

    rconn.flushdb()

    # init
    ni = NgramIndexer(prefix, rconn, accepted_extensions)

    # read
    path = './text'
    ni.read_from_path(path)
    ni.clean_low_freqs_entry()

    # print ngrams countss
    print 'Years: ', ni.years
    print 'Files: ', ni.files
    for year in ni.years:
        print 'Found %s ngrams in year %s books' % (
            rconn.zcard(prefix + 'ngrams:%s' % year), year)
