#!/usr/bin/env bash
# Chang Liu <changliu@qinggukeji.com>

export REDIS_DB_ADDR=ngrams.egto1w.0001.usw2.cache.amazonaws.com
export NGRAM_ENVIRONMENT=prod

apt-get update
apt-get install -y python python-pip python-virtualenv nginx gunicorn supervisor

pip install -r requirements.txt
python -c "import nltk; nltk.download(all)"

rm /etc/nginx/sites-enabled/default
cp deploy/flask.conf /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/flask.conf /etc/nginx/sites-enabled/flask.conf
echo "daemon off;" >> /etc/nginx/nginx.conf

mkdir -p /var/log/supervisor
cp deploy/gunicorn.conf /etc/supervisor/conf.d/
cp deploy/supervisord.conf /etc/supervisor/conf.d/

/usr/bin/supervisord