# Minimal Google Book Ngram Viewer #

## Data parsing and ngram generating ##

Without considering supporting multi language other than English, the txt file
are read with codecs with UTF-8 decoding. BOM caused me a bit trouble at first
but was recognized and addressed soon. Ngrams are generated using NLTK by first
tokenize content into words than run nltk.ngrams on the words. To avoid having
ngrams across punctuations, the content were first tokenized by the punctuations
using nltk.regexp_tokenizer, than tokenized to a list of list of words using
word_tokenizer. Ngrams generated from each list of words are then combined.

## Data structure for indexing ##

Currently the indexing of the ngrams is stored purely using redis’ sorted set.
Two types of indices are stored.  A group of indices store the occurrence of
ngrams by year, which is used to query the distributions of occurrence over the
years The other stores the summed count of each ngram for auto-completion and
auto-correction.

#### Pros ####

* The structure is fairly straightforward.  The query is very fast since every
* lookup is O(1).  The implementation is scalable because Redis can be easily
* sharded.

#### Cons ####

* Though it can be scale, it’s quite expansive.  There’s quite a lot of overhead
* storing the index as it is. Some of the overhead can be eliminated by indexing
* the word first. I end up didn’t implement that.

#### Other options ####

* The first thought has to be to use tree/trie since it’s intuitive to think of
* the ngrams as a tree where each node is a word and each path represents a
* ngram. Literature says Ternary AVL tree is the most efficient structure. But I
* end up didn’t implement one myself. The second thought is to use frameworks
* for text indexing, such as Lucene or ElasticSearch. I played with the Whoosh —
* the python counterpart of Lucene — for a while and decided it’s not fun to use
* something like that.

## Frontend ##

I use Flask + bootstrap for a lightweight frontend, also add vanilla
jquery/javascript here and there to move thing around, definitely not very
pretty. But the UI has the basic functions including the search box and auto
completion indicator and autocorrection message and the result chart, which is
generated using Morris chart.

## Deployment ##

The service is now deployed on an AWS EC2 t2.micro instance, with 4 gunicorn
workers. The redis database is serving from a AWS elastic cache instance.
Because the t2.micro instance has very limited computational power, the indexing
is done on a local machine and the redis dump is saved to a S3 bucket and loaded
to the elastic cache instance at init.

## Thing I would do

* Dockerize the service, so that is scalable and less painful to deploy Allow
* user to upload text for async indexing, which requires using celery in the
* backend for running async tasks Refine the indexing structure, though I was
* pretty amazed by how much I can do with Redis only this implementation is
* still quite hacky. I wouldn’t rely on it to handle TBs of texts. Add support
* for other languages including character languages (yes, I’m talking about
* Chinese)

